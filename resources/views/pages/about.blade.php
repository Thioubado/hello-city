@extends('app')
  


@section('title')
About-us | {{config('app.name')}} 
@endsection


@section('content')

   <p>Built with &hearts; by LES TEACHERS DU NET.</p>

   <p><a href="{{route('home')}}">Retour à la page d'accueil</a></p>

@endsection