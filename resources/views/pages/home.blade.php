@extends('app')

@section('title')
    {{config('app.name')}}
@endsection

@section('content')

    <h1>Hello from Casablanca</h1>

    <p>It's currently {{date('h:i A')}} </p>

@endsection
